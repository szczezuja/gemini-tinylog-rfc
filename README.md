# RFC: TinyLogs format

Status: Draft
Last update: 2021-06-08

## Introduction

A Tinylog is a simple file with all "microblog" style entries to share small contents and interact with other other geminauts' tinylog.

The original idea and most "rules" comes from Drew/uoou/Friendo:

=> gemini://friendo.monster/log/lace.gmi

The goal of this RFC is to standardize the tinylog format for better coherence between authors by providing a consistent approach.
This will also allow better tools around tinylogs to improve both authors and readers experience reading micro entries.


## Tinylog format

This tinylog format proposal is composed of a mandatory part that tinylogs must be compatible with and optional information that author could add as meta information about their tinylog.


### Mandatory format

Each tinylog entry must be formated as follow:
```
## <Date> <Entry Title>
<Content>
```

It starts with a second level header (`##`), followed by a space and then `<date>` followed by a space and then the `<Entry Title>`.

`<Entry Title>` format: Any text without new line.

`<Date>` format: 3 possible formats for the date:

```
YYYY-MM-DD h:m[:s] TZ
Weekday DD Month YYYY h:m[:s] TZ # format 1-24 for hours
Weekday DD Month YYYY h:m[:s] AM TZ # format 1-12 am/pm for hours
```

Entries should be in order from newest to oldest. 

**YYYY-MM-DD h:m[:s] TZ**

Seconds are optional.

example:
```
2006-01-02 15:04:05 MST
2006-01-02 15:04 MST
```


**Weekday DD Month YYYY h:m[:s] TZ**

* Weekday should be in 3 letters format (eg: Mon for Monday).
* Month should be in 3 letters format (eg: Jan for January).
* Hours should be in 1-24 format.
* Seconds are optional.

example:
```
Mon 02 Jan 2006 15:04:05 MST
Mon 02 Jan 2006 15:04 MST
```

**Weekday DD Month YYYY h:m[:s] am TZ**

* Weekday should be in 3 letters format (eg: Mon for Monday).
* Month should be in 3 letters format (eg: Jan for January).
* Hours should be in 1-12 format.
* Seconds are optional.

example:
```
Mon 02 Jan 2006 03:04:05 PM MST
Mon 02 Jan 2006 03:04 PM MST
```

`<Content>` format: Any text without 2 new lines.

Each entry must be separated with 2 new lines (ie paragraph break).

Example:
```
## Mon 07 Jun 2021 10:44:26 PM CEST
A small thought to share.

## Sun 06 Jun 2021 11:44:26 AM CEST
A first tinylog entry
```

### Optional information

Additional informations could be added at the top of your tinylog file:
```
# <Tinylog Title>

<description>

author: <author>
avatar: <avatar>
license: <license>
```

`<Tinylog Title>` Can be any text without a line break.

`<description>`: Can be any text with a line breaks. 

`<author>` format: "@authorName@capsule.tld" with authorName the tinylog author name and "capsule.tld" the url of the author capsule. This intends to simplify communication between authors as well as discovery of new tinylog feeds.
Example:
```
author: @bacardi55@gmi.bacardi55.io
```

`<avatar>`: 1 emoji or 1 letter. Can be followed by a space and an optional text (eg: to indicate the code for the emoji)
example:
```
avatar: 🐘
avatar: 🦪 (:oyster:, U+1F9AA)
```

`<license>`: Can be any text without a line break.
```
license: CC BY-SA
```

Informations order is negligible but <description> shouldn't be mixed with rest of structured informations. 
